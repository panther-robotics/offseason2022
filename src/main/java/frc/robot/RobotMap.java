package frc.robot;

public class RobotMap {

    //xbox controller:

    /**Xbox controller port. */
    public static final int XBOX_PORT = 3;

    //wheel ports:

    /**Front left motor port. */
    public static final int FRONT_LEFT_MOTOR = 2;
    /**Back left motor port. */
    public static final int BACK_LEFT_MOTOR = 3;
    /**Front right motor port. */
    public static final int FRONT_RIGHT_MOTOR = 4;
    /**Back right motor port */
    public static final int BACK_RIGHT_MOTOR = 5;

    //other motor ports:

    /**PigeonIMU port. */
    public static final int PigeonIMU = 12;
    
    /**Climb motor port. */
    public static final int LeftClimberMotor = 13;
    public static final int RightClimberMotor = 14;

    public static final int BackSpinMotor = 7;

    /**Collector motor port. */
    public static final int leftCollectMotor = 11;
    public static final int rightCollectMotor = 16;

    /**Left shooter motor port. */
    public static final int shooterLeftMotor = 9;
    /**Right shooter motor port. */
    public static final int shooterRightMotor = 10;

    /**Trigger port. */
    public static final int trigger = 8;

    /**Back Spin Motor */

    /*hall effect sensor port*/
    public static final int climb_hall_effect = 0;

    //PCM ports:

    /**Left collector solenoid port. */
    public static final int CollectorSolenoidLeft = 1;
    /**Right collector solenoid port. */
    public static final int CollectorSolenoidRight = 0;




    /**Pixy2 port. */
    public static final int Pixy2_Ball_Collector = 0x54;



    /**USB ports */
    public static final int TopCam = 0;
}