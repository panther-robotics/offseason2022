package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.sensors.PigeonIMU;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.RobotMap;
import frc.robot.Robot;

public class Drivetrain extends SubsystemBase {

    public final static double TICKS_PER_INCH = 1047.8667194;

    WPI_TalonFX frontLeftMotor = new WPI_TalonFX(RobotMap.FRONT_LEFT_MOTOR);
    WPI_TalonFX frontRightMotor = new WPI_TalonFX(RobotMap.FRONT_RIGHT_MOTOR);
    WPI_TalonFX backLeftMotor = new WPI_TalonFX(RobotMap.BACK_LEFT_MOTOR);
    WPI_TalonFX backRightMotor = new WPI_TalonFX(RobotMap.BACK_RIGHT_MOTOR);

    PigeonIMU pigeon = new PigeonIMU(RobotMap.PigeonIMU);

    private final DifferentialDrive drive = new DifferentialDrive(frontLeftMotor, frontRightMotor);

    public static DifferentialDriveKinematics driveKinematics = new DifferentialDriveKinematics(
            Units.inchesToMeters(26.3));
    DifferentialDriveOdometry odometry = new DifferentialDriveOdometry(getHeading()); // come back

    SimpleMotorFeedforward feedforward = new SimpleMotorFeedforward(Constants.ksVolts, Constants.kvVoltSecondsPerMeter,
            Constants.kaVoltSecondsSquaredPerMeter);

    PIDController leftPID = new PIDController(Constants.kP, 0, 0);
    PIDController rightPID = new PIDController(Constants.kP, 0, 0);

    Pose2d pose;

    public Drivetrain() {
        backLeftMotor.follow(frontLeftMotor);
        backRightMotor.follow(frontRightMotor);

        frontRightMotor.setInverted(false);
        frontLeftMotor.setInverted(true);

        frontLeftMotor.setSelectedSensorPosition(0);
        frontRightMotor.setSelectedSensorPosition(0);

    }
    
    public Rotation2d getHeading() {
        return Rotation2d.fromDegrees(pigeon.getYaw());
    }

    public double getRightPosition() {
        return Units.inchesToMeters(frontRightMotor.getSelectedSensorPosition() / TICKS_PER_INCH);
    }

    public double getLeftPosition() {
        return Units.inchesToMeters(frontLeftMotor.getSelectedSensorPosition() / TICKS_PER_INCH);
    }

    public SimpleMotorFeedforward getFeedforward() {
        return feedforward;
    }

    public PIDController getLeftPID() {
        return leftPID;
    }

    public PIDController getRightPID() {
        return rightPID;
    }

    public Pose2d getPose(){
        return pose;
    }

    public static void resetOdometry(Pose2d initialPose) {
    }

    public void outputVolts(Double leftVolts, Double rightVolts) {
        frontLeftMotor.setVoltage(leftVolts); 
        frontRightMotor.setVoltage(rightVolts);
        drive.feed();
      }

    public DifferentialDriveWheelSpeeds getWheelSpeeds(){
     return new DifferentialDriveWheelSpeeds(
        (Units.inchesToMeters(frontLeftMotor.getSelectedSensorPosition() / TICKS_PER_INCH)),
        Units.inchesToMeters(frontRightMotor.getSelectedSensorPosition() / TICKS_PER_INCH));
    }
    @Override
    public void periodic() {
        pose = odometry.update(getHeading(), getLeftPosition(), getRightPosition());
        SmartDashboard.putNumber("LeftDrivetrainEncoder", frontLeftMotor.getSelectedSensorPosition());
        SmartDashboard.putNumber("RightDrivetrainEncoder", frontRightMotor.getSelectedSensorPosition());
    }

}