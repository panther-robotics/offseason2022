// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean
 * constants. This class should not be used for any other purpose. All constants
 * should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    // feedforward variables
    public static final double trackWidth = 26.3;

    public static final double maxSpeed = 0.5; // meters per second
    public static final double maxAcceleration = 0.5; // meters per second squared
    public static final double maxAMetersPerSecond = 0.5;

    public static final double RamseteB = 2;
    public static final double RamseteZeta = 0.7;

    public static final double ksVolts = 0.64082;
    public static final double kvVoltSecondsPerMeter = 2.0198;
    public static final double kaVoltSecondsSquaredPerMeter = 0.44801;
    public static final double kP = 2.9668;

}
